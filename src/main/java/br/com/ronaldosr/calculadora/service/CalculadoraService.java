package br.com.ronaldosr.calculadora.service;

import br.com.ronaldosr.calculadora.DTO.RespostaDTO;
import br.com.ronaldosr.calculadora.model.Calculadora;
import org.springframework.stereotype.Service;

@Service
public class CalculadoraService {

    public RespostaDTO somar(Calculadora calculadora) {
        int resultado = 0;
        for (Integer numero: calculadora.getNumeros()) {
            resultado = resultado + numero;
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

    public RespostaDTO subtrair(Calculadora calculadora) {
        int resultado = 0;
        for (Integer numero: calculadora.getNumeros()) {
            if (resultado == 0) {
                resultado = resultado + numero;
            } else {
                resultado = resultado - numero;
            }
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

    public RespostaDTO multiplicar(Calculadora calculadora) {
        int resultado = 0;
        for (Integer numero: calculadora.getNumeros()) {
            if (resultado == 0) {
                resultado = resultado + numero;
            } else {
                resultado = resultado * numero;
            }
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

    public RespostaDTO dividir(Calculadora calculadora) {
        int resultado = 0;
        for (Integer numero: calculadora.getNumeros()) {
            if (resultado == 0) {
                resultado = resultado + numero;
            } else {
                resultado = resultado / numero;
            }
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }
}
