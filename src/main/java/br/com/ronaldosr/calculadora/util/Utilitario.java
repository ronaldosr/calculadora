package br.com.ronaldosr.calculadora.util;

import br.com.ronaldosr.calculadora.model.Calculadora;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Component
public class Utilitario {
    public static void verificarQtdNumerosLista(Calculadora calculadora) {
        if (calculadora.getNumeros().size() < 1) {
            // Excessão exclusiva de controllers
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "São necessários, no mínimo, dois números para somar.");
        }
    }

    public static void verificarNegativoLista(Calculadora calculadora) {
        for (int i = 0; i < calculadora.getNumeros().size(); i++) {
            int j = i  + 1;
            if (calculadora.getNumeros().get(i) < 0) {
                throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "O " + j  + "º número da lista é negativo. " +
                        "Por favor, corrigir.");
            }
        }
    }

    public static void verificarDivisaoPorZero(Calculadora calculadora) {
        for (int i = 1; i < calculadora.getNumeros().size(); i++) {
            if (calculadora.getNumeros().get(i) == 0) {
                int j = i + 1;
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Não é possível dividir por zero! " +
                        "Por favor, corrigir o " + j + "º número da lista.");
            }
        }
    }
}
