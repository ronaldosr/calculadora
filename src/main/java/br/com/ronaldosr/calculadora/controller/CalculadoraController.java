package br.com.ronaldosr.calculadora.controller;

import br.com.ronaldosr.calculadora.DTO.RespostaDTO;
import br.com.ronaldosr.calculadora.model.Calculadora;
import br.com.ronaldosr.calculadora.service.CalculadoraService;
import br.com.ronaldosr.calculadora.util.Utilitario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

// @RequestMapping para definir o recurso
@RestController
@RequestMapping("/calculadora")
public class CalculadoraController {

    @Autowired
    CalculadoraService calculadoraService;

    @Autowired
    Utilitario util;

    @PostMapping("/somar")
    public RespostaDTO somar(@RequestBody Calculadora calculadora) {
        util.verificarQtdNumerosLista(calculadora);
        util.verificarNegativoLista(calculadora);

        return calculadoraService.somar(calculadora);
    }

    @PostMapping("/subtrair")
    public RespostaDTO subtrair(@RequestBody Calculadora calculadora) {
        util.verificarQtdNumerosLista(calculadora);
        util.verificarNegativoLista(calculadora);

        return calculadoraService.subtrair(calculadora);
    }

    @PostMapping("/multiplicar")
    public RespostaDTO multiplicar(@RequestBody Calculadora calculadora) {
        util.verificarQtdNumerosLista(calculadora);
        util.verificarNegativoLista(calculadora);

        return calculadoraService.multiplicar(calculadora);
    }

    @PostMapping("/dividir")
    public RespostaDTO dividir(@RequestBody Calculadora calculadora) {
        util.verificarQtdNumerosLista(calculadora);
        util.verificarNegativoLista(calculadora);
        util.verificarDivisaoPorZero(calculadora);

        return calculadoraService.dividir(calculadora);
    }
}
